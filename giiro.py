from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash
from flask_sqlalchemy import SQLAlchemy
import json
import sys

app = Flask(__name__, static_path='/static')
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/giiro.db'
db = SQLAlchemy(app)

class Marcador(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	lat = db.Column(db.Float)
	lng = db.Column(db.Float)
	
	def __init__(self, lat, lng):
		self.lat = lat
		self.lng = lng
	
	def __repr(self):
		return '<Marcador lat: %f, lng: %f>' % self.lat, self.lng

@app.route("/")
def exibePaginaInicial():
	return "<a href='static/index.html'>GIIRO</a>"

@app.route("/markers")
def listaMarcadores():
	return json.dumps([[m.lat, m.lng, m.id] for m in Marcador.query.all()])

@app.route("/markers", methods=['POST'])
def adicionaMarcador():
	global db
	
	coords = json.loads(request.data.decode("utf-8"))
	m = Marcador(coords[0], coords[1])
	db.session.add(m)
	db.session.commit()
	
	return json.dumps({'indice': m.id}), 200, {'ContentType': 'application/json'}

@app.route("/editMarker/<int:indice>", methods=['POST'])
def editarMarcador(indice):
	global db
	
	coords = json.loads(request.data.decode("utf-8"))
	m = Marcador.query.get(indice)
	m.lat = coords[0]
	m.lng = coords[1]
	db.session.add(m)
	db.session.commit()
	
	return '', 200

@app.route("/removeMarker/<int:indice>", methods=['POST'])
def excluirMarcador(indice):
	global db
	
	m = Marcador.query.get(indice)
	db.session.delete(m)
	db.session.commit()
	
	return '', 200

if __name__ == "__main__":
	app.debug = True
	app.run()
