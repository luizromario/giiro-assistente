var map = L.map('map').setView([-12.96, -38.47], 12);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

map.on('click', clicouNoMapa);
carregaMarcadores();

//////////////

function carregaMarcadores() {
	$.get('/markers', function(data) {
		list = JSON.parse(data);
		for (var i = 0; i < list.length; i++) {
			var coords = list[i];
			adicionaMarcador(new L.LatLng(coords[0], coords[1]), coords[2]);
		}
	});
}

function editarMarcador(indice, latlng) {
	$.ajax({
		url: '/editMarker/' + indice,
		type: 'POST',
		contentType: 'application/json',
		dataType: 'json',
		data: JSON.stringify([latlng.lat, latlng.lng])
	});
}

function excluirMarcador(indice) {
	$.ajax({
		url: '/removeMarker/' + indice,
		type: 'POST'
	});
}

function adicionaMarcador(latlng, indice) {
	var marker = L.marker(latlng, {draggable: true});
	marker.on('popupopen', abrePopup);
	marker.on('dragend', function (e) {
		editarMarcador(indice, e.target._latlng);
	});
	marker.on('remove', function () {
		excluirMarcador(indice);
	});
	marker.addTo(map)
		.bindPopup("<a class='apagar' href='#'>Apagar</a>");
}

function clicouNoMapa(e) {
	$.ajax({
	    url: '/markers',
		type: 'POST',
		contentType: 'application/json',
	    dataType: 'json',
		data: JSON.stringify([e.latlng.lat, e.latlng.lng]),
		success: function (data) {
			adicionaMarcador(e.latlng, data.indice);
		}
	})
}

function abrePopup() {
	var marker = this;
	$('.apagar').click(function() {
		map.removeLayer(marker);
	});
}
